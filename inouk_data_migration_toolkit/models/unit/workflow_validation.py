# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Cyril MORISSE - Audaxis ( @cmorisse )
#    Copyright 2014 Audaxis
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime
from openerp import netsvc, SUPERUSER_ID
from openerp.addons.connector.queue.job import job
from openerp.addons.connector.unit.synchronizer import ImportSynchronizer
from ..backend import idmt_10csv
from ..connector import get_environment


_logger = logging.getLogger("IDMTWorkflowBatchValidator")


"""
Workflow Batch Validation for Inouk Data Migration Toolkit.
"""


class InoukLoaderException(RuntimeError):
    """ Base Exception for the connectors """
    pass


class IDMTBatchProcessor(ImportSynchronizer):
    """
    Base importer for InoukDataMigrationToolkit
    """

    def __init__(self, environment):
        """
        :param environment: current environment (backend, session, ...)
        :type environment: :py:class:`connector.connector.Environment`
        """
        super(IDMTBatchProcessor, self).__init__(environment)

    def run(self,  file_name):
        """
        import one file
        """
        pass


@idmt_10csv
class IDMTWorkflowBatchValidator(IDMTBatchProcessor):
    """
    Workflow Batch Validator
    Automate asynchronous workflow validation of large sets of data
    """
    _model_name = 'idmt.backend'

    def run(self, model, domain, signal, offset, limit):
        """
        Processing
        """
        wf_service = netsvc.LocalService('workflow')

        uid = SUPERUSER_ID
        job_result = ''

        message = "Validation batch '%s' started" % ([model, domain, signal, offset, limit],)
        _logger.info(message)
        job_result += "%s\n" % message

        begin_time = datetime.now()

        object_ids = self.session.search(model, domain, offset=offset, limit=limit, order='id')
        for object_id in object_ids:
            wf_service.trg_validate(uid, model, object_id, signal, self.session.cr)

        end_time = datetime.now()
        duration = end_time - begin_time

        message = "Validation batch '%s' processed in %s seconds" % ([model, domain, signal, offset, limit], duration,)
        _logger.info(message)
        job_result += "%s\n" % message

        if self.backend_record.commit_after_each_file:
            message = "Validation batch '%s': commit() called after validation." % ([model, domain, signal, offset, limit],)
            _logger.info(message)
            job_result += "%s\n" % message
            self.session.cr.commit()

        return job_result


@job
def workflow_validation_batch(session, backend_id, model, domain, signal, offset, limit):
    """
    Launch a Validation batch (job or direct)
    """
    env = get_environment(session, 'idmt.backend', backend_id)
    batch_validator = env.get_connector_unit(IDMTWorkflowBatchValidator)
    job_result = batch_validator.run(model, domain, signal, offset, limit)
    return job_result
