# -*- coding: utf-8 -*-
##############################################################################
#
#    Author: Cyril MORISSE - Audaxis
#    Copyright 2013 Camptocamp SA
#    Copyright 2013 Akretion
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime
import os
from openerp.osv import osv, fields, orm
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.addons.connector.session import ConnectorSession
from openerp.tools.translate import _
from .unit.import_synchronizer import csv_file_import_batch
from .unit.workflow_validation import workflow_validation_batch
from openerp.tools.safe_eval import safe_eval


_logger = logging.getLogger('InoukDataMigrationToolkit')


class IDMTBackend(orm.Model):
    _name = 'idmt.backend'
    _description = 'Inouk Data Migration Toolkit Backend'
    _inherit = 'connector.backend'

    _backend_type = 'inoukdatamigrationtoolkit'

    def _select_versions(self, cr, uid, context=None):
        """ Available versions

        Can be inherited to add custom versions.
        """
        return [("1.0 CSV", "Inouk Data Migration Toolkit - 1.0 CSV")]

    _columns = {
        'name': fields.char("Name", size=64,
                            help="Arbitrary unique name for this backend. A backend can be seen as "
                                 "an import file directory."),
        'description': fields.text("Description"),
        'version': fields.selection(_select_versions, string='Version', required=False),
        'direct_mode': fields.boolean(_("Direct/debug mode"),
                                      help="If checked, OpenERP won't launch a job. but will run the import"
                                           " immediately"),
        'source_files_directory': fields.char("Source files directory", size=256, required=True,
                                              help="Path on the server where the files will be "
                                                   "retrieved. Default value is ok."),

        # add a field `auto_activate` -> activate a cron
        'import_files_list': fields.text(_("Import files list"),
                                         help="Specify files to import on per line. Don't write "
                                              "path, just file names. Files will be loaded from "
                                              "'Source file directory' above."),
        'commit_after_each_file': fields.boolean(_("Commit after each file"),
                                                 help="If checked, idmt will issue a database commit after each file. "
                                                      "Hence only file who failed to load will need to be reloaded"),
        'no_update': fields.boolean(_("No update"),
                                    help="If checked, OpenERP will prevent any update of the objects loaded in these"
                                         " files. For that, it uses the external identifier you define in the id "
                                         "column"),
        'last_import_launch_timestamp': fields.datetime("Last import launch time"),
        'default_lang_id': fields.many2one('res.lang', 'Default Language',
                                           help="If a default language is selected, the records "
                                                "will be imported in the translation of this "
                                                "language.\n"),
        # for delete
        'delete_object_list': fields.text(_("Delete object list"),
                                          help="Specify list of objects to delete (one per line). Format is "
                                               "'module_name:object.name'. For object use odoo dotted "
                                               "notation as returned by debug mode. (Eg. product.product)"),
        'commit_after_each_object_deletion': fields.boolean(_("Commit after each object deletion"),
                                                            help="If checked, idmt will issue a database commit after "
                                                                 "each line is processed."),
        'externalid_object_list': fields.text(_("External id object list"),
                                              help="Specify list of objects you want to generate external id for. Format is "
                                                   "'model:domain:expression'. For model use odoo dotted "
                                                   "notation (Eg. product.product)"),

        # Workflow batch validation list
        'wkf_batch_valication_list': fields.text(_("Workflow batch validation list")),

    }

    _defaults = {
        'source_files_directory':  '../../files_to_import/',
        'direct_mode': False,
        'no_update': False,
        'commit_after_each_file': True,
        'commit_after_each_object_deletion': True,
    }
    _sql_constraints = []

    def create(self, cr, uid, values, context=None):
        """
        We create import directory when it is either created or modified in backend
        :return:
        """
        retval = super(IDMTBackend, self).create(cr, uid, values, context=context)
        if not retval:
            return

        if values.get("source_files_directory", False) and not os.path.exists(values['source_files_directory']):
            os.makedirs(values['source_files_directory'])
        return retval

    def write(self, cr, uid, ids, values, context=None):
        """
        We create import directory when it is either created or modified in backend
        :return:
        """
        retval = super(IDMTBackend, self).write(cr, uid, ids, values, context=context)
        if not retval:
            return

        if values.get("source_files_directory", False) and not os.path.exists(values['source_files_directory']):
            os.makedirs(values['source_files_directory'])
        return retval

    def launch_import_job(self, cr, uid, ids, context=None):
        """
        Create a Job to import files specified in import_files_list
        :param cr:
        :param uid:
        :param ids: ids of the backend on which the method is called
        :param context:
        :return:
        """
        if not hasattr(ids, '__iter__'):
            ids = [ids]

        session = ConnectorSession(cr, uid, context=context)
        import_launch_time = datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        for backend_brws in self.browse(cr, uid, ids, context=context):
            if backend_brws.direct_mode:
                csv_file_import_batch(session, backend_brws.id, backend_brws.import_files_list)
            else:
                csv_file_import_batch.delay(session, backend_brws.id, backend_brws.import_files_list)

        return self.write(cr, uid, ids, {'last_import_launch_timestamp': import_launch_time})

    def launch_delete_job(self, cr, uid, ids, context=None):
        """
        Create a Job to delete  objetcs specified in delete_objects_list
        :param cr:
        :param uid:
        :param ids: ids of the backend on which the method is called
        :param context:
        :return:
        """

        # TODO: implement a job launcher
        # TODO: delete object per qty eg. delete per 250 objects and commit after each delete
        if not hasattr(ids, '__iter__'):
            ids = [ids]

        for backend_brws in self.browse(cr, uid, ids, context=context):
            # No job for now if backend_brws.direct_mode:
                delete_object_list = filter(None, backend_brws.delete_object_list.split('\n'))

                for delete_object in delete_object_list:
                    module, object_name = delete_object.split(':')

                    try:
                        to_delete_obj = self.pool.get(object_name)
                    except Exception:
                        raise Exception("Delete Error", "%s is not a valid object name." % object_name)

                    query = """SELECT res_id
                             FROM ir_model_data
                             WHERE module = %s AND model = %s ;"""
                    cr.execute(query, (module, object_name,))
                    sql_result = cr.fetchall()
                    target_ids = [elem[0] for elem in sql_result]

                    to_delete_obj.unlink(cr, uid, target_ids)

                    if backend_brws.commit_after_each_object_deletion:
                        cr.commit()

        return True

    def get_or_create_external_id(self, cr, uid, model, res_id, module, prefix_expr, context=None):
        model_obj = self.pool.get(model)
        object_brws = model_obj.browse(cr, uid, res_id, context=context)

        ir_model_data_obj = self.pool.get('ir.model.data')
        external_ids = ir_model_data_obj.search(cr, uid, [('res_id', '=', res_id), ('model', '=', model)], context=context)

        if not prefix_expr:
            prefix_expr = '"%s__%s" % (o._model._table, o.id,)'

        if external_ids:
            # TODO: if several returns the one iwth module if it exists
            return external_ids[0]
        else:
            xml_id = safe_eval(prefix_expr, {}, dict(o=object_brws))
            return ir_model_data_obj.create(cr, uid, {
                'name': xml_id,
                'model': model,
                'module': module,
                'res_id': res_id,
                }, context=context)

    def map_select_record_with_xmlid(self, cr, uid, model, domain, module, id_generator_expr, context=None):
        model_osv = self.pool.get(model)
        model_ids = model_osv.search(cr, uid, domain, context=context)
        for model_id in model_ids:
            self.get_or_create_external_id(cr, uid, model, model_id, module, id_generator_expr, context=context)

    def launch_external_id_generation_job(self, cr, uid, ids, context=None):
        if not hasattr(ids, '__iter__'):
            ids = [ids]

        for backend_brws in self.browse(cr, uid, ids, context=context):
            map_object_list = backend_brws.externalid_object_list.replace('\n', '|||')
            map_object_list = filter(None, map_object_list.split('|||'))
            for map_object in map_object_list:
                splited_map_object = map_object.split(':')

                if splited_map_object[0].strip() == '':
                    continue

                if len(splited_map_object) != 4:
                    raise osv.except_osv("Mapping job error ! Mapping syntax must be model:domain:prefix_expr")

                model, domain, module, id_generator_expr = splited_map_object
                parsed_domain = safe_eval(domain.strip())
                self.map_select_record_with_xmlid(cr, uid, model.strip(), parsed_domain, module, id_generator_expr.strip(), context=context)
        return True

    def launch_workflow_validation_job(self, cr, uid, ids, context=None):
        if not hasattr(ids, '__iter__'):
            ids = [ids]

        session = ConnectorSession(cr, uid, context=context)
        for backend_brws in self.browse(cr, uid, ids, context=context):
            workflow_validation_list = backend_brws.wkf_batch_valication_list.replace('\n', '|||')
            workflow_validation_list = filter(None, workflow_validation_list.split('|||'))
            for workflow_chunk in workflow_validation_list:
                splited_workflow_chunk = workflow_chunk.split(':')

                if splited_workflow_chunk[0].strip() == '':
                    continue

                if len(splited_workflow_chunk) != 5:
                    raise osv.except_osv("Syntax error", "Fix line '%s'" % workflow_chunk)

                model, domain, signal, offset, limit = splited_workflow_chunk
                parsed_domain = safe_eval(domain.strip())
                parsed_offset = eval(offset)
                parsed_limit = eval(limit)

                if backend_brws.direct_mode:
                    workflow_validation_batch(session, backend_brws.id,
                                              model,
                                              parsed_domain,
                                              signal,
                                              parsed_offset,
                                              parsed_limit)
                else:
                    workflow_validation_batch.delay(session, backend_brws.id,
                                                    model,
                                                    parsed_domain,
                                                    signal,
                                                    parsed_offset,
                                                    parsed_limit)

        return True

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
